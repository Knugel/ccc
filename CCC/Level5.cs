using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using g3;

namespace CCC
{
    public class Level5 : Level
    {
        public const float EPSILON = (float) 10e-5;
        
        protected override void Run(string[] lines, TextWriter output)
        {
            var transferRange = float.Parse(lines[0]);
            var count = int.Parse(lines[1]);
            var cells = new List<Plane>();
            
            for (var i = 1; i <= count + 1; i++)
            {
                var split = lines[i].Split(' ');
                var cell = new Plane();
                cell.flightId = int.Parse(split[0]);
                cells.Add(cell);
            }

            var rangeSquared = transferRange * transferRange;

            foreach (var planeA in cells)
            {
                var flightsA = GetFlights(planeA.flightId)
                    .Where(x => x.alt > 6000);
                var pointsA = flightsA.Select(x =>
                    new Point
                    {
                        Flight = x,
                        Position = latLonToEcef(x.lat, x.lang, x.alt)
                    });

                foreach (var planeB in cells)
                {
                    var flightsB = GetFlights(planeB.flightId);
                    if (flightsA.First().destination == flightsB.First().destination)
                        break;
                    var pointsB = flightsB.Select(x =>
                        new Point
                        {
                            Flight = x,
                            Position = latLonToEcef(x.lat, x.lang, x.alt)
                        });


                    foreach (var pA in pointsA)
                    {
                        var possiblePoints = new Dictionary<int, Level5Output>();
                        
                        for (var t = 1; t <= 3600; t++)
                        {
                            possiblePoints[t] = new Level5Output()
                            {
                                Points = new List<(Point, Point)>(),
                                PlaneA = planeA,
                                PlaneB = planeB
                            };
                            
                            foreach (var pB in pointsB)
                            {
                                var dist = pA.Position.DistanceSquared(pB.Position);
                                if (dist > 1000000 && dist < rangeSquared &&
                                    (pA.Flight.timestamp == pB.Flight.timestamp + t))
                                {
                                    possiblePoints[t].Points.Add((pA, pB));
                                }
                            }
                        }

                        foreach (var entry in possiblePoints
                            .OrderBy(x => x.Value.PlaneA.flightId)
                            .ThenBy(x => x.Value.PlaneB.flightId)
                            .ThenBy(x => x.Key))
                        {
                            if(!entry.Value.Points.Any())
                                continue;
                            
                            var range = entry.Value.Points.First().Item1.Flight.timestamp.ToString();
                            if (entry.Value.Points.Count > 1)
                            {
                                var last = entry.Value.Points.First().Item1.Flight.timestamp;
                                range += last;
                                bool isRange = false;
                                
                                foreach (var point in entry.Value.Points)
                                {
                                    var ts = point.Item1.Flight.timestamp;
                                    if (ts == last + 1 || ts == last)
                                    {
                                        last = ts;
                                        isRange = ts != last;
                                    }
                                    else
                                    {
                                        if (isRange)
                                            range += $"-{last} {ts}";
                                        else
                                            range += $" {ts}";
                                    }
                                }
                            }

                            output.WriteLine($"{entry.Value.PlaneA.flightId} {entry.Value.PlaneB.flightId} {entry.Key} {range}");
                        }
                    }
                }
            }

            output.Flush();
        }

        Vector3d latLonToEcef(double lat, double lon, double alt)
        {   
            double clat = Math.Cos(lat * (Math.PI / 180));
            double slat = Math.Sin(lat * (Math.PI / 180));
            double clon = Math.Cos(lon * (Math.PI / 180));
            double slon = Math.Sin(lon * (Math.PI / 180));

            double WGS84_A = 6371000.0;
            double WGS84_E = 0;
            
            double N = WGS84_A / Math.Sqrt(1.0 - WGS84_E * WGS84_E * slat * slat);

            var x = (N + alt) * clat * clon;
            var y = (N + alt) * clat * slon;
            var z = (N * (1.0 - WGS84_E * WGS84_E) + alt) * slat;
            return new Vector3d(x, y, z);
        }
        
        public List<Flight> GetFlights(int flightId)
        {
            var lines = File.ReadAllLines($"flights\\{flightId}.csv");
            var start = lines[0];
            var dest = lines[1];
            var takeoff = int.Parse(lines[2]);
            var count = int.Parse(lines[3]);
            var ret = new List<Flight>();
            
            
            for (var i = 4; i <= count + 3; i++)
            {
                var split = lines[i].Split(',');
                var flight = new Flight();
                flight.destination = dest;
                flight.takeoff = takeoff;
                flight.origin = start;
                flight.timestampOffset = int.Parse(split[0]);
                flight.lat = double.Parse(split[1]);
                flight.lang = double.Parse(split[2]);
                flight.alt = double.Parse(split[3]);
                ret.Add(flight);
            }

            return ret;
        }
        
        double Lerp(double firstFloat, double secondFloat, double by)
        {
            return firstFloat * (1 - by) + secondFloat * by;
        }

        public class Plane
        {
            public int flightId;

            public int timestamp;
        }

        public class Flight
        {
            public string origin;
            public string destination;
            public int takeoff;
            public int countCoordinates;
            public int timestampOffset;
            public double lat;
            public double lang;
            public double alt;

            public int timestamp => takeoff + timestampOffset;
        }

        public class Point
        {
            public Vector3d Position;
            public Flight Flight;
        }

        public class Level5Output
        {
            public Plane PlaneA;
            public Plane PlaneB;
            public List<(Point, Point)> Points;
        }
    }
}