using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CCC
{
    public class Level1 : Level
    {
        public const float EPSILON = (float) 10e-5;
        
        protected override void Run(string[] lines, TextWriter output)
        {
            var count = int.Parse(lines[0]);
            var cells = new List<Cell>();
            
            for (var i = 1; i <= count; i++)
            {
                var split = lines[i].Split(',');
                var cell = new Cell();
                cell.timestamp = int.Parse(split[0]);
                cell.lat = float.Parse(split[1]);
                cell.lang = float.Parse(split[2]);
                cell.alt = float.Parse(split[3]);
                cells.Add(cell);
            }

            var minLang = cells.Min(x => x.lang);
            var maxLang = cells.Max(x => x.lang);

            var minLat = cells.Min(x => x.lat);
            var maxLat = cells.Max(x => x.lat);
            
            var minTs = cells.Min(x => x.timestamp);
            var maxTs = cells.Max(x => x.timestamp);

            var minAlt = 0;
            var maxAlt = cells.Max(x => x.alt);
            
            output.WriteLine($"{minTs} {maxTs}");
            output.WriteLine($"{minLat} {maxLat}");
            output.WriteLine($"{minLang} {maxLang}");
            output.WriteLine($"{maxAlt}");
            output.Flush();
        }
        
        public class Cell
        {
            public int timestamp;

            public float lat;

            public float lang;

            public float alt;
        }
    }
}