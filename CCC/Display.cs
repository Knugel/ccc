using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;
using g3;

namespace CCC
{
    public class Display : Form
    {
        private List<(object, Color)> _shapes;

        public Display()
        {
            _shapes = new List<(object, Color)>();
        }

        public void Add(object shape)
        {
            _shapes.Add((shape, Color.Black));
        }

        public void Add(object shape, Color color)
        {
            _shapes.Add((shape, color));
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            var g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            
            foreach (var shape in _shapes)
            {
                var pen = new SolidBrush(shape.Item2);
                switch(shape.Item1)
                {
                    case Circle2d circle:
                        var x = (int)circle.Center.x;
                        var y = (int) circle.Center.y;
                        var size = (int)(circle.Radius * 2);
                        g.FillEllipse(pen, x, y, size, size);
                        break;
                    case Box2d box:
                        x = (int)(box.Center.x - box.Extent.x);
                        y = (int)(box.Center.y - box.Extent.y);
                        var width = (int)box.Extent.x * 2;
                        var height = (int) box.Extent.y * 2;
                        g.FillRectangle(pen, x, y, width, height);
                        break;
                    case Vector2d point:
                        g.FillRectangle(pen, (int)point.x, (int)point.y, 1, 1);
                        break;
                    case PolyLine2d line:
                        var points = line.Vertices.Select(v => new PointF((float)v.x, (float)v.y)).ToArray();
                        g.DrawLines(new Pen(shape.Item2), points);
                        break;
                    case Polygon2d polygon:
                        points = polygon.Vertices.Select(v => new PointF((float)v.x, (float)v.y)).ToArray();
                        g.FillPolygon(pen, points);
                        break;
                    default:
                        Console.WriteLine($"Unknown shape: {shape.GetType().FullName}");
                        break;
                }
            }
        }
    }
}