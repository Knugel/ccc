using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using g3;

namespace CCC
{
    public class Level3 : Level
    {
        public const float EPSILON = (float) 10e-5;
        
        protected override void Run(string[] lines, TextWriter output)
        {
            var count = int.Parse(lines[0]);
            var cells = new List<Cell>();
            
            for (var i = 1; i <= count; i++)
            {
                var split = lines[i].Split(',');
                var cell = new Cell();
                cell.lat = double.Parse(split[0]);
                cell.lang = double.Parse(split[1]);
                cell.alt = double.Parse(split[2]);
                cells.Add(cell);
            }

            foreach (var cell in cells)
            {
                var vec = latLonToEcef(cell.lat, cell.lang, cell.alt);
                output.WriteLine($"{vec.x} {vec.y} {vec.z}");
            }
            
            output.Flush();
        }
        
        Vector3d latLonToEcef(double lat, double lon, double alt)
        {   
            double clat = Math.Cos(lat * (Math.PI / 180));
            double slat = Math.Sin(lat * (Math.PI / 180));
            double clon = Math.Cos(lon * (Math.PI / 180));
            double slon = Math.Sin(lon * (Math.PI / 180));

            double WGS84_A = 6371000.0;
            double WGS84_E = 0;
            
            double N = WGS84_A / Math.Sqrt(1.0 - WGS84_E * WGS84_E * slat * slat);

            var x = (N + alt) * clat * clon;
            var y = (N + alt) * clat * slon;
            var z = (N * (1.0 - WGS84_E * WGS84_E) + alt) * slat;
            return new Vector3d(x, y, z);
        }
        
        public class Cell
        {
            public double lat;

            public double lang;

            public double alt;
        }
    }
}