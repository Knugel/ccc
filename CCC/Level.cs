using System.IO;
using System.Linq;

namespace CCC
{
    public abstract class Level
    {
        private const string PatternIn = "level{0}_{1}.in";
        private const string PatternOut = "{0}\\output_{0}_{1}";
        
        protected virtual int Amount => 6;

        public void RunAll()
        {
            var id = GetType().FullName.Last();
            for (var i = 1; i <= Amount; i++)
            {
                var lines = File.ReadAllLines(string.Format(PatternIn, id, i));
                
                Directory.CreateDirectory(id.ToString());
                using(var output = File.Create(string.Format(PatternOut, id, i)))
                {
                    Run(lines, new StreamWriter(output));
                    output.Flush(true);
                }
            }
        }

        protected abstract void Run(string[] lines, TextWriter output);
    }
}