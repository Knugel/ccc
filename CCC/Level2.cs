using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CCC
{
    public class Level2 : Level
    {
        protected override void Run(string[] lines, TextWriter output)
        {
            var count = int.Parse(lines[0]);
            var cells = new List<Cell>();
            
            for (var i = 1; i <= count; i++)
            {
                var split = lines[i].Split(',');
                var cell = new Cell();
                cell.timestamp = int.Parse(split[0]);
                cell.lat = float.Parse(split[1]);
                cell.lang = float.Parse(split[2]);
                cell.alt = float.Parse(split[3]);
                cell.start = split[4];
                cell.destination = split[5];
                cell.takeoff = int.Parse(split[6]);
                cells.Add(cell);
            }

            var c = 0;
            foreach (var cell in cells.GroupBy(x => $"{x.start}{x.destination}")
                .OrderBy(x => x.First().start)
                .ThenBy(x => x.First().destination))
            {
                var key = cell.First();
                var flightCount = cell.GroupBy(x => x.takeoff).Select(x => x.First()).Count();
                output.WriteLine($"{key.start} {key.destination} {flightCount}");
                c += flightCount;
            }

            Console.WriteLine(c);
            output.Flush();
        }
        
        public class Cell
        {
            public int timestamp;

            public float lat;

            public float lang;

            public float alt;

            public string start;

            public string destination;

            public int takeoff;
        }
        
        public class CellComparer : IEqualityComparer<Cell>
        {
            public bool Equals(Cell x, Cell y)
            {
                return x.start == y.start && x.destination == y.destination && x.takeoff == y.takeoff;
            }

            public int GetHashCode(Cell obj)
            {
                return obj.GetHashCode();
            }
        }
    }
}