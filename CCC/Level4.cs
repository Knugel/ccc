using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using g3;

namespace CCC
{
    public class Level4 : Level
    {
        public const float EPSILON = (float) 10e-5;
        
        protected override void Run(string[] lines, TextWriter output)
        {
            var count = int.Parse(lines[0]);
            var cells = new List<Plane>();
            
            for (var i = 1; i <= count; i++)
            {
                var split = lines[i].Split(' ');
                var cell = new Plane();
                cell.flightId = int.Parse(split[0]);
                cell.timestamp = int.Parse(split[1]);
                cells.Add(cell);
            }

            foreach (var plane in cells)
            {
                var flights = GetFlights(plane.flightId);
                var ordered = flights.OrderBy(x => x.takeoff + x.timestampOffset).ToList();
                var ret = FindFlight(ordered, plane.timestamp);
                output.WriteLine(ret);
            }
            
            output.Flush();
        }

        public string FindFlight(List<Flight> flights, int ts)
        {
            Flight last;
            Flight next;
            int lastTs;
            int nextTs;
            double by;
            double alt;
            double lat;
            double lang;

            for (var i = 0; i < flights.Count; i++)
            {
                var current = flights[i];
                var timestamp = current.takeoff + current.timestampOffset;
                if (timestamp >= ts)
                {
                    last = flights[Math.Max(i - 1, 0)];
                    next = flights[i];
                    lastTs = last.takeoff + last.timestampOffset;
                    nextTs = next.takeoff + next.timestampOffset;
                    by = (ts - (double)lastTs) / (nextTs - (double)lastTs);

                    alt = Lerp(last.alt, next.alt, @by);
                    lat = Lerp(last.lat, next.lat, @by);
                    lang = Lerp(last.lang, next.lang, @by);
                        
                    return ($"{lat} {lang} {alt}");
                }
            }

            throw new Exception("Fuck you");
        }

        public List<Flight> GetFlights(int flightId)
        {
            var lines = File.ReadAllLines($"flights\\{flightId}.csv");
            var start = lines[0];
            var dest = lines[1];
            var takeoff = int.Parse(lines[2]);
            var count = int.Parse(lines[3]);
            var ret = new List<Flight>();
            
            
            for (var i = 4; i <= count + 3; i++)
            {
                var split = lines[i].Split(',');
                var flight = new Flight();
                flight.destination = dest;
                flight.takeoff = takeoff;
                flight.origin = start;
                flight.timestampOffset = int.Parse(split[0]);
                flight.lat = double.Parse(split[1]);
                flight.lang = double.Parse(split[2]);
                flight.alt = double.Parse(split[3]);
                ret.Add(flight);
            }

            return ret;
        }
        
        double Lerp(double firstFloat, double secondFloat, double by)
        {
            return firstFloat * (1 - by) + secondFloat * by;
        }
        
        double LerpImprecise (double v0, double v1, double t) {
            return v0 + t * (v1 - v0);
        }

        
        public class Plane
        {
            public int flightId;

            public int timestamp;
        }

        public class Flight
        {
            public string origin;
            public string destination;
            public int takeoff;
            public int countCoordinates;
            public int timestampOffset;
            public double lat;
            public double lang;
            public double alt;
        }
    }
}