using System;
using System.Drawing;
using g3;

namespace CCC
{
    public static class Extensions
    {
        public const float EPSILON = (float) 10e-5;
        
        public static Box2d ToBox(this RectangleF rectangle)
        {
            var center = new Vector2d(rectangle.X + rectangle.Width / 2, rectangle.Y + rectangle.Height / 2);
            var extents = new Vector2d(rectangle.Width / 2, rectangle.Height / 2);
            return new Box2d(center, extents);
        }
        
        public static Box2d ToBox(this Rectangle rectangle)
        {
            return new RectangleF(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height).ToBox();
        }

        public static bool Equals(this float a, float b)
        {
            return a.Compare(b) == 0;
        }
        
        public static int Compare(this float a, float b)
        {
            var diff = a - b;
            if (Math.Abs(diff) <= EPSILON)
            {
                return 0;
            }
            else if (diff > 0)
            {
                // A is greater than B
                return 1;
            }
            else
            {
                // A is less than B
                return -1;
            }
        }
    }
}